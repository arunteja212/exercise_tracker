This project is about building a modern single-page application with popular stack of technologies named MERN stack 

The MERN stack is involved with following technologies:
MongoDB: A document-based database.
Express: A web application framework for Node.js.
React: A JavaScript front-end library for building user interfaces.
Node.js: JavaScript run-time environment that executes JavaScript code outside of a browser (such as a server).



## Frontend Scripts

In the project directory consists of "src" dealing with React , you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

##  Backend Scripts

In the project directory consists of "backend" dealing with Express Server and Mangoos for data modelling  , you can run:

### `npm init -y`

create a package.json file

### `npm install express cors mongoose dotenv`

install the depedencies like, Express is a fast and lightweight web framework for Node.js, cors package provides an Express middleware that can enable CORS with different options, mongoose makes interacting with MongoDB through Node.js, dotenv loads environment variables from a .env file into process.env.  

### `npm install -g nodemon`

install nodemon makes development easier. It is a tool that helps create and update Node.js based applications.

### `nodemon server`

Runs the Server in the development mode.<br />
Open [http://localhost:5000](http://localhost:5000)